<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Lab extends Model
{
    protected $fillable = ['training_partner_id', 'title', 'description', 'capacity', 'is_ac', 'is_projector'];

    public function TrainingPartner()
    {
        return $this->belongsTo('App\TrainingPartner');
    }
}
