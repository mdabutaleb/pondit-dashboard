<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Institute extends Model
{
 protected $fillable = ['title', 'description', 'logo', 'banner', 'address', 'display'];
}
