<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TrainerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        return [
            'title' => 'required|max:50|min:4',
            'img' => 'required|mimes:jpeg,jpg,bmp,png,gif',
            'cv' => 'required|mimes:doc,docx,pdf',
        ];
    }

    public function messages()
    {
        return [
            'title.required' => "Trainer Name can't be empty",
            'title.min' => "Trainer Name Must be between 4 - 50 character",
            'cv.required' => 'Plase upload CV before submit',
            'cv.mimes' => 'Only .doc, .docx & .pdf file is allow to upload'
        ];
    }
}
