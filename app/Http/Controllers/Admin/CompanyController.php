<?php

namespace App\Http\Controllers\Admin;

use App\Company;
use const Couchbase\ENCODER_COMPRESSION_NONE;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Intervention\Image\Facades\Image;

class CompanyController extends Controller
{


    public function index()
    {
        $comapnies = Company::whereDisplay('YES')->paginate(5);
        return view(Auth::user()->service . '.company.index')->with(['companies' => $comapnies]);

    }

    public function create()
    {
        $view = Auth::user()->service . '.company.create';
        return view($view);
    }


    public function store(Request $request)
    {
        $data = $request->except(['logo', 'banner']);
        $company = Company::create($data);
        $this->imageUpload($request->all(), $company);
        Session::flash('flash_message', 'Company successfully added. ');

        return redirect()->back();
    }

    public function show($id)
    {
        echo "i am going to show something";
    }

    public function edit($id)
    {
        $company = Company::find($id);
        return view(Auth::user()->service . '.company.edit', compact('company'));
    }

    public function update(Request $request, $id)
    {
        $data = $request->except(['logo', 'banner']);
        $update = Company::find($id);
        $update->update($data);
        $this->imageUpload($request->all(), $update);
        Session::flash('flash_message', 'Company successfully Updated. ');
        return redirect()->back();

    }

    public function destroy($id)
    {

        $delete = Company::find($id);
        $path = "img/company/" . $delete->title;
        system('/bin/rm -rf ' . escapeshellarg($path));
        $delete->delete();
        Session::flash('flash_message', 'Company successfully Deleted. ');
        return redirect()->back();
    }

    public function imageUpload($request, $company)
    {
        foreach ($request as $key => $value) {
            if (Input::file($key)) {
                $form_image = Input::file($key);
                $slug = ($key == 'logo') ? 'logo' : 'banner';
                $New_image_Name = $slug . "." . $form_image->getClientOriginalExtension();
                $Dynamic_path = config('appconfig.company_path') . "$company->title/";
                $file_path = $Dynamic_path . $New_image_Name;

                if (!\File::isDirectory($Dynamic_path)) {
                    \File::makeDirectory($Dynamic_path, 493, true);
                }
                if ($key == 'logo') {
                    Image::make($form_image->getRealPath())->resize(350, 140)->save($file_path);
                } else {
                    Image::make($form_image->getRealPath())->resize(1400, 250)->save($file_path);
                }

                $company = Company::find($company->id);
                $company->update([$key => $file_path]);
            }
        }
    }

}
