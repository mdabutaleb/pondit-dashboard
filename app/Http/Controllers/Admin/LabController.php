<?php

namespace App\Http\Controllers\Admin;

use App\Company;
use App\Http\Controllers\Controller;
use App\Lab;
use App\TrainingPartner;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class LabController extends Controller
{

    public function index()
    {
        $labs = Lab::with('TrainingPartner')->paginate(10);
        return view(Auth::user()->service . '.lab.index', compact('labs'));
    }


    public function create()
    {
        $TrainingPartners = TrainingPartner::whereDisplay('YES')->pluck('title', 'id');
        return view(Auth::user()->service . '.lab.create', compact('TrainingPartners'));
    }

    public function store(Request $request)
    {
//        dd($request->all());
        $data = $request->except('_token');
        $newLab = Lab::create($data);
        Session::flash('key', 'value');
        Session::get('key');
        Session::flash('flash_message', 'Lab successfully added. ');
        return redirect()->back();
    }


    public function show(Lab $lab)
    {

    }


    public function edit(Lab $lab)
    {
        $TrainingPartners = TrainingPartner::whereDisplay('YES')->pluck('title', 'id');
        $data = [
            'TrainingPartners' => $TrainingPartners,
            'lab' => $lab,
        ];
        return view(Auth::user()->service . '.lab.edit', $data);
    }


    public function update(Request $request, Lab $lab)
    {
        $data = $request->except('_token');

        $lab->update($data);
        Session::flash('flash_message', 'Lab successfully Updated. ');
        return redirect()->back();

    }


    public function destroy(Lab $lab)
    {
        $lab->delete();
        Session::flash('flash_message', 'Lab successfully Deleted. ');
        return redirect()->back();
    }
}
