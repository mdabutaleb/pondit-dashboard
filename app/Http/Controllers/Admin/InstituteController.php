<?php

namespace App\Http\Controllers\Admin;

use App\Company;
use App\Institute;
use const Couchbase\ENCODER_COMPRESSION_NONE;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Illuminate\Validation\Rules\In;
use Intervention\Image\Facades\Image;

class InstituteController extends Controller
{

    public function index()
    {
        $institutes = Institute::whereDisplay('YES')->paginate(10);
        return view(Auth::user()->service . '.institute.index')->with(['institutes' => $institutes]);
    }

    public function create()
    {
        $view = Auth::user()->service . '.institute.create';
        return view($view);
    }


    public function store(Request $request)
    {
        $data = $request->except(['logo', 'banner']);
        $institute = Institute::create($data);
        $this->imageUpload($request->all(), $institute);
        Session::flash('flash_message', 'Institute successfully added. ');

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Institute $institute
     * @return \Illuminate\Http\Response
     */
    public function show(Institute $institute)
    {
        //
    }

    public function edit(Institute $institute)
    {
        return view(Auth::user()->service . '.institute.edit', compact('institute'));
    }

    public function update(Request $request, Institute $institute)
    {
        $data = $request->except(['logo', 'banner']);
        $update = $institute;
        $update->update($data);
        $this->imageUpload($request->all(), $update);
        Session::flash('flash_message', 'Institute successfully Updated. ');
        return redirect()->back();
    }


    public function destroy($id)
    {

        $delete = Institute::find($id);
        $path = "img/institute/" . $delete->title;
        system('/bin/rm -rf ' . escapeshellarg($path));
        $delete->delete();
        Session::flash('flash_message', 'Institute successfully Deleted. ');
        return redirect()->back();
    }

    public function imageUpload($request, $institute)
    {
        foreach ($request as $key => $value) {
            if (Input::file($key)) {
                $form_image = Input::file($key);
                $slug = ($key == 'logo') ? 'logo' : 'banner';
                $New_image_Name = $slug . "." . $form_image->getClientOriginalExtension();
                $Dynamic_path =  'img/institute/'. "$institute->title/";
                $file_path = $Dynamic_path . $New_image_Name;

                if (!\File::isDirectory($Dynamic_path)) {
                    \File::makeDirectory($Dynamic_path, 493, true);
                }
                if ($key == 'logo') {
                    Image::make($form_image->getRealPath())->resize(350, 140)->save($file_path);
                } else {
                    Image::make($form_image->getRealPath())->resize(1400, 250)->save($file_path);
                }

                $company = Institute::find($institute->id);
                $company->update([$key => $file_path]);
            }
        }
    }

}
