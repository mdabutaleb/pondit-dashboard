<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\TrainerRequest;
use App\Trainer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Intervention\Image\Facades\Image;

class TrainerController extends Controller
{

    public function index()
    {
        $trainers = Trainer::paginate(10);
        return view(Auth::user()->service . '.trainer.index', compact('trainers'));
    }


    public function create()
    {
        $designations = ['1' => 'Lead Trainer', '2' => 'Assistant Trainer'];
        return view(Auth::user()->service . '.trainer.create', compact('designations'));
    }


    public function store(TrainerRequest $request)
    {

        $data = $request->except(['img', 'cv']);
        $Trainer = Trainer::create($data);
        $this->FileUPload($request->all(), $Trainer);
        Session::flash('flash_message', 'Trainer successfully added. ');
        return redirect()->back();
    }

    public function show(Trainer $trainer)
    {

    }

    public function edit(Trainer $trainer)
    {
        $designations = ['1' => 'Lead Trainer', '2' => 'Assistant Trainer'];
        $data = [
            'designations' => $designations,
            'trainer' => $trainer,
        ];
        return view(Auth::user()->service . '.trainer.edit', $data);
    }


    public function update(Request $request, Trainer $trainer)
    {
        $this->validate($request, [
            'title' => 'required|max:50',
        ]);

        $data = $request->except(['img', 'cv']);
        $trainer->update($data);
        if (isset($request['cv'])) {
            $p = $request['cv']->getPathName();
            $this->FileUPload($request->all(), $trainer, $p);
        } else {
            $this->FileUPload($request->all(), $trainer);
        }


        Session::flash('flash_message', 'Trainer successfully updated. ');
        return redirect()->back();
    }


    public function destroy(Trainer $trainer)
    {
        $path = "img/trainer/" . $trainer->title;
        system('/bin/rm -rf ' . escapeshellarg($path));
        $trainer->delete();
        Session::flash('flash_message', 'Trainer successfully Deleted. ');
        return redirect()->back();
    }

    public function FileUPload($request, $trainer, $p = '')
    {
        foreach ($request as $key => $value) {
            if (Input::file($key)) {
                $file = Input::file($key);
                $slug = ($key == 'img') ? 'profile-pic' : time() . 'cv';
                $New_image_Name = $slug . "." . $file->getClientOriginalExtension();
                $Dynamic_path = config('appconfig.trainer_path') . "$trainer->title/";
//                dd($Dynamic_path);
                $file_path = $Dynamic_path . $New_image_Name;

                if (!\File::isDirectory($Dynamic_path)) {
                    \File::makeDirectory($Dynamic_path, 493, true);
                }
                if ($key == 'img') {
                    Image::make($file->getRealPath())->resize(200, 230)->save($file_path);
                } else {
                    move_uploaded_file($file->getPathName(), $file_path);
                }

                $Update = Trainer::find($trainer->id);
                $Update->update([$key => $file_path]);
            }
        }
    }

}
