<?php

namespace App\Http\Controllers\Admin;

use App\Course;
use App\Trainer;
use App\TrainingPartner;
use function GuzzleHttp\Psr7\str;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;

class CourseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $courses = Course::with('leadTrainer', 'asstTrainer', 'training_partner')->paginate(10);
//        dd($courses);
        return view(Auth::user()->service . '.course.index', compact('courses'));
    }

    public function create()
    {
        $TrainingPartners = TrainingPartner::whereDisplay('YES')->pluck('title', 'id');
        $LeadTrainers = Trainer::whereDesignation(1)->pluck('title', 'id');
        $AssistantTrainer = Trainer::whereDesignation(2)->pluck('title', 'id');
        $data = [
            'TrainingPartners' => $TrainingPartners,
            'LeadTrainers' => $LeadTrainers,
            'AssistantTrainer' => $AssistantTrainer,
        ];
        return view(Auth::user()->service . '.course.create', $data);

    }

    public function store(Request $request)
    {
//        dd($request->all());

        $data = $request->all();
        $course = Course::create($data);
        $this->FileUPload($request->all(), $course);
        Session::flash('flash_message', 'Course added successfully. ');
        return redirect()->back();

    }

    public function show(Course $course)
    {

        return view(Auth::user()->service . '.course.show', compact('course'));
    }

    public function edit(Course $course)
    {
        $TrainingPartners = TrainingPartner::whereDisplay('YES')->pluck('title', 'id');
        $LeadTrainers = Trainer::whereDesignation(1)->pluck('title', 'id');
        $AssistantTrainer = Trainer::whereDesignation(2)->pluck('title', 'id');
        $data = [
            'TrainingPartners' => $TrainingPartners,
            'LeadTrainers' => $LeadTrainers,
            'AssistantTrainer' => $AssistantTrainer,
            'course' => $course,
        ];
        return view(Auth::user()->service . '.course.edit', $data);

    }


    public function update(Request $request, Course $course)
    {
        $data = $request->all();
        $course->update($data);
        $this->FileUPload($request->all(), $course);
        Session::flash('flash_message', 'Course successfully Updated. ');
        return redirect()->back();

    }

    public function destroy(Course $course)
    {
//        dd('I am going to delete someting');
        $course->delete();
        Session::flash('flash_message', 'Course successfully deleted. ');
        return redirect()->back();
    }

    public function FileUPload($request, $course, $p = '')
    {
        foreach ($request as $key => $value) {
            if (Input::file($key)) {
                $file = Input::file($key);
                $slug = str_slug($course->title, '-');
                $New_image_Name = $slug . "." . $file->getClientOriginalExtension();
                $Dynamic_path = 'upload/course/' . "$course->title/";
//                dd($Dynamic_path);
                $file_path = $Dynamic_path . $New_image_Name;

                if (!\File::isDirectory($Dynamic_path)) {
                    \File::makeDirectory($Dynamic_path, 493, true);
                }

                move_uploaded_file($file->getPathName(), $file_path);


                $Update = Course::find($course->id);
                $Update->update([$key => $file_path]);
            }
        }
    }

}
