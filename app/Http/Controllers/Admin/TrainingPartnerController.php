<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\User;
use const Couchbase\ENCODER_COMPRESSION_NONE;
use App\TrainingPartner;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Intervention\Image\Facades\Image;
use DB;

class TrainingPartnerController extends Controller
{
    public function index()
    {

//        $partners = TrainingPartner::whereDisplay('YES')->paginate(10);
        $partners = TrainingPartner::paginate(10);
        return view(Auth::user()->service . '.training-partner.index')->with(['partners' => $partners]);

    }

    public function create()
    {
        $view = Auth::user()->service . '.training-partner.create';
        return view($view);
    }


    public function store(Request $request)
    {

        DB::beginTransaction();

        try {
            $userInfo = $request->only(['email', 'password']);
            $userInfo['name'] = $request->title;
            $userInfo['service'] = 'tp';
            $user = User::create($userInfo);

            $data = $request->except(['logo', 'banner']);
            $data['user_id'] = $user->id;
            $partner = TrainingPartner::create($data);
            $this->imageUpload($request->all(), $partner);
            Session::flash('flash_message', 'Training Partner added successfully. ');

            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
        }


        return redirect()->back();
    }

    public function show($id)
    {
        echo "i am going to show something";
    }

    public function edit($id)
    {
        $partner = TrainingPartner::find($id);
        return view(Auth::user()->service . '.training-partner.edit', compact('partner'));
    }

    public function update(Request $request, $id)
    {
        $data = $request->except(['logo', 'banner']);
        $update = TrainingPartner::find($id);
        $update->update($data);
        $this->imageUpload($request->all(), $update);
        Session::flash('flash_message', 'Training Partner Updated Successfully');
        return redirect('admin/training-partner');

    }

    public function destroy($id)
    {

        $delete = TrainingPartner::find($id);
        $path = "img/partner/" . $delete->title;
        system('/bin/rm -rf ' . escapeshellarg($path));
        $delete->delete();
        Session::flash('flash_message', 'Company successfully Deleted. ');
        return redirect()->back();
    }

    public function imageUpload($request, $partner)
    {
        foreach ($request as $key => $value) {
            if (Input::file($key)) {
                $form_image = Input::file($key);
                $slug = ($key == 'logo') ? 'logo' : 'banner';
                $New_image_Name = $slug . "." . $form_image->getClientOriginalExtension();
                $Dynamic_path = 'img/partner/' . "$partner->title/";
                $file_path = $Dynamic_path . $New_image_Name;

                if (!\File::isDirectory($Dynamic_path)) {
                    \File::makeDirectory($Dynamic_path, 493, true);
                }
                if ($key == 'logo') {
                    Image::make($form_image->getRealPath())->resize(350, 140)->save($file_path);
                } else {
                    Image::make($form_image->getRealPath())->resize(1400, 250)->save($file_path);
                }

                $company = TrainingPartner::find($partner->id);
                $company->update([$key => $file_path]);
            }
        }
    }
}
