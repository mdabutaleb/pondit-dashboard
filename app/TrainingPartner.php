<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TrainingPartner extends Model
{
    protected $fillable = ['user_id', 'title', 'description', 'address', 'display', 'logo', 'banner'];

    public function lab()
    {
        return $this->hasMany('App\Lab');
    }

    public function course()
    {
        return $this->hasMany('App\Course');
    }
}
