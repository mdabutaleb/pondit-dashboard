<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    protected $fillable = ['title', 'description', 'training_partner_id', 'training_partner_id', 'outline', 'start_date', 'end_date', 'lead_trainer', 'asst_trainer', 'course_length'];

    protected $with = ['training_partner', 'leadTrainer', 'asstTrainer'];

    public function setStartDateAttribute($date)
    {
        $this->attributes['start_date'] = date('Y-m-d', strtotime($date));
    }

    public function setEndDateAttribute($enddate)
    {
        $this->attributes['end_date'] = date('Y-m-d', strtotime($enddate));

    }

    public function training_partner()
    {
        return $this->belongsTo('App\TrainingPartner');
    }


    public function leadTrainer()
    {
        return $this->belongsTo('App\Trainer', 'lead_trainer');
    }

    public function asstTrainer()
    {
        return $this->belongsTo('App\Trainer', 'asst_trainer');
    }

}
