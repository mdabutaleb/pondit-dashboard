<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Trainer extends Model
{
    protected $fillable = ['title', 'description', 'designation', 'img', 'mobile', 'email', 'web', 'git', 'cv'];

    public function coursesAsLeadTrainer()
    {
        return $this->hasMany('App\Course', 'lead_trainer');
    }

    public function courseAsAsstTrainer()
    {
        return $this->hasMany('App\Course', 'asst_trainer');
    }
}
