<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('/login');
});
Route::get('/clear-cache', function() {
    $exitCode = Artisan::call('cache:clear');
    // return what you want
});

Auth::routes();

Route::middleware('auth')->group(function () {
    Route::get('/dashboard', 'Admin\DashboardController@index')->name('home');
    Route::resource('admin/company', 'Admin\CompanyController');
    Route::resource('admin/training-partner', 'Admin\TrainingPartnerController');
    Route::resource('admin/institute', 'Admin\InstituteController');
    Route::resource('admin/lab', 'Admin\LabController');
    Route::resource('admin/trainer', 'Admin\TrainerController');
    Route::resource('admin/course', 'Admin\CourseController');
});