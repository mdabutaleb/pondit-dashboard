<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCoursesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('courses', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->text('description')->nullable();
            $table->integer('training_partner_id')->nullable()->unsigned();
            $table->string('outline')->nullable();
            $table->date('start_date')->nullable();
            $table->date('end_date')->nullable();
            $table->integer('lead_trainer')->nullable()->unsigned();
            $table->integer('asst_trainer')->nullable()->unsigned();
            $table->string('course_length')->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('training_partner_id')->references('id')->on('training_partners')->onDelete('cascade');
            $table->foreign('lead_trainer')->references('id')->on('trainers')->onDelete('cascade');
            $table->foreign('asst_trainer')->references('id')->on('trainers')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('courses');
    }
}
