function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#logo')
                .attr('src', e.target.result)
                .width(270)
                .height(150);
        };

        reader.readAsDataURL(input.files[0]);
    }
}
function BannerImage(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#banner')
                .attr('src', e.target.result)
                .width(270)
                .height(150);
        };

        reader.readAsDataURL(input.files[0]);
    }
}
function ProfleRewview(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#profile')
                .attr('src', e.target.result)
                .width(200)
                .height(230);
        };

        reader.readAsDataURL(input.files[0]);
    }
}
