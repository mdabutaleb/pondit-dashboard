<ul id="slide-out" class="side-nav fixed sn-bg-1 custom-scrollbar">
    <!-- Logo -->
    <li>
        <div class="user-box">
            <img src="https://mdbootstrap.com/img/Photos/Avatars/avatar-5.jpg" class="img-fluid rounded-circle">
            <p class="user text-center black-text">Jane Doe</p>
        </div>
    </li>
    <!--/. Logo -->
    <!-- Side navigation links -->
    <li>
        <ul class="collapsible collapsible-accordion">
            <li><a class="collapsible-header waves-effect arrow-r"><i class="fa fa-"></i> Dashboards<i class="fa fa-angle-down rotate-icon"></i></a>
                <div class="collapsible-body">
                </div></li>
            <li><a href="analytics.html" class="collapsible-header waves-effect arrow-r"><i class="fa fa-pie-chart"></i> Analytics</a>
            <li><a class="collapsible-header waves-effect arrow-r"><i class="fa fa-code"></i> Creators<i class="fa fa-angle-down rotate-icon"></i></a>
                <div class="collapsible-body">
                    <ul>
                        <li><a href="modals.html" class="waves-effect">Modals</a>
                        </li>
                        <li><a href="page-create.html" class="waves-effect">Create Page</a>
                        </li>
                    </ul>
                </div>
            </li>
            <li><a class="collapsible-header waves-effect arrow-r"><i class="fa fa-lock"></i> Forms<i class="fa fa-angle-down rotate-icon"></i></a>
                <div class="collapsible-body">
                    <ul>
                        <li><a href="signup.html" class="waves-effect">Sign up</a>
                        </li>
                        <li><a href="signup%20v2.html" class="waves-effect">Sign up v2</a>
                        </li>
                        <li><a href="login.html" class="waves-effect">Login</a>
                        </li>
                        <li><a href="editaccount.html" class="waves-effect">Edit Account</a>
                        </li>
                    </ul>
                </div>
            </li>
            <li><a class="collapsible-header waves-effect arrow-r"><i class="fa fa-bar-chart"></i> SEO<i class="fa fa-angle-down rotate-icon"></i></a>
                <div class="collapsible-body">
                    <ul>
                        <li><a href="seo-overview.html" class="waves-effect">Overview</a>
                        </li>
                    </ul>
                </div>
            </li>
            <li><a href="clients.html" class="collapsible-header waves-effect arrow-r"><i class="fa fa-users"></i> Clients</a>
            <li><a href="invoice.html" class="collapsible-header waves-effect arrow-r"><i class="fa fa-money"></i> Invoice</a>
            <li><a href="support.html" class="collapsible-header waves-effect arrow-r"><i class="fa fa-support"></i> Support</a>
            <li><a href="faq.html" class="collapsible-header waves-effect arrow-r"><i class="fa fa-question-circle" aria-hidden="true"></i> FAQ</a>
        </ul>
    </li>
    <!--/. Side navigation links -->
    <div class="sidenav-bg mask-strong"></div>
</ul>