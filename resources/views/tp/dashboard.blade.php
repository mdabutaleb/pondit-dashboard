@extends('tp.layouts.master')
@section('bread_crumb')
    {{--<li class="breadcrumb-item active">My location</li>--}}
@endsection
@section('statistics')

    <!--Carousel Wrapper-->
    <div id="carousel-example-1z" class="carousel slide carousel-fade" data-ride="carousel">
        <div class="carousel-inner" role="listbox">
            <!--First slide-->
            <div class="carousel-item active">
                <img src="{{ asset('img/bitm/bitm.png') }}" alt="First slide">
            </div>
            <!--/First slide-->
        </div>
    </div>
    <!--/.Carousel Wrapper-->

    <!--Rotating card-->
    <!--/.Rotating card-->

        <!--First row-->
        <div class="row">
            <div class="col-md-3 md-1">
                <!--Rotating card-->
                <a class="rotate-btn" data-card="card-1">
                    <div class="card-wrapper2">
                        <div id="card-1" class="card-rotating effect__click">
                            <!--Front Side-->
                            <div class="face front">

                                <div class="card-block">
                                    <br/>
                                    <h2>বি আই টি এম শিক্ষার্থী</h2>
                                    {{--<p>শিক্ষার্থী</p>--}}
                                    {{--<!--Triggering button-->--}}
                                    {{--<p>--}}
                                    {{--অধ্যয়নরত সকল ছাত্র ছাত্রীদের সর্বশেষ খবর পেতে এখানে ক্লিক করুন।--}}
                                    {{--</p>--}}

                                </div>


                            </div>
                            <!--/.Front Side-->

                            <!--Back Side-->
                            <a class="rotate-btn" data-card="card-1">
                                <div class="face back">

                                    <!--Content-->
                                    <h4>About me</h4>

                                    <hr>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Maxime quae, dolores
                                        dicta. </p>
                                    <hr>
                                    <!--Social Icons-->
                                    <ul class="inline-ul">
                                        <li><a class="icons-sm fb-ic"><i class="fa fa-facebook"></i></a></li>
                                        <li><a class="icons-sm tw-ic"><i class="fa fa-twitter"></i></a></li>
                                        <li><a class="icons-sm gplus-ic"><i class="fa fa-google-plus"></i></a></li>
                                        <li><a class="icons-sm li-ic"><i class="fa fa-linkedin"></i></a></li>
                                    </ul>
                                    <!--Triggering button-->


                                </div>
                            </a>
                            <!--/.Back Side-->

                        </div>
                    </div>
                </a>
            </div>
            <div class="col-md-3 md-1">
                <!--Rotating card-->
                <a class="rotate-btn" data-card="card-2">
                    <div class="card-wrapper2">
                        <div id="card-2" class="card-rotating effect__click">
                            <!--Front Side-->
                            <div class="face front">

                                <div class="card-block">
                                    <br/>
                                    <h2>ববর্তমান কোর্সের অবস্থা</h2>
                                    {{--<p>শিক্ষার্থী</p>--}}
                                    {{--<!--Triggering button-->--}}
                                    {{--<p>--}}
                                    {{--অধ্যয়নরত সকল ছাত্র ছাত্রীদের সর্বশেষ খবর পেতে এখানে ক্লিক করুন।--}}
                                    {{--</p>--}}

                                </div>


                            </div>
                            <!--/.Front Side-->

                            <!--Back Side-->
                            <a class="rotate-btn" data-card="card-2">
                                <div class="face back">

                                    <!--Content-->
                                    <h4>About me</h4>

                                    <hr>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Maxime quae, dolores
                                        dicta. </p>
                                    <hr>
                                    <!--Social Icons-->
                                    <ul class="inline-ul">
                                        <li><a class="icons-sm fb-ic"><i class="fa fa-facebook"></i></a></li>
                                        <li><a class="icons-sm tw-ic"><i class="fa fa-twitter"></i></a></li>
                                        <li><a class="icons-sm gplus-ic"><i class="fa fa-google-plus"></i></a></li>
                                        <li><a class="icons-sm li-ic"><i class="fa fa-linkedin"></i></a></li>
                                    </ul>
                                    <!--Triggering button-->


                                </div>
                            </a>
                            <!--/.Back Side-->

                        </div>
                    </div>
                </a>
            </div>
            <div class="col-md-3 md-1">
                <!--Rotating card-->
                <a class="rotate-btn" data-card="card-3">
                    <div class="card-wrapper2">
                        <div id="card-3" class="card-rotating effect__click">
                            <!--Front Side-->
                            <div class="face front">

                                <div class="card-block">
                                    <br/>
                                    <h2> সকল শিক্ষার্থীদের ফলাফল</h2>
                                    {{--<p>শিক্ষার্থী</p>--}}
                                    {{--<!--Triggering button-->--}}
                                    {{--<p>--}}
                                    {{--অধ্যয়নরত সকল ছাত্র ছাত্রীদের সর্বশেষ খবর পেতে এখানে ক্লিক করুন।--}}
                                    {{--</p>--}}

                                </div>


                            </div>
                            <!--/.Front Side-->

                            <!--Back Side-->
                            <a class="rotate-btn" data-card="card-3">
                                <div class="face back">

                                    <!--Content-->
                                    <h4>About me</h4>

                                    <hr>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Maxime quae, dolores
                                        dicta. </p>
                                    <hr>
                                    <!--Social Icons-->
                                    <ul class="inline-ul">
                                        <li><a class="icons-sm fb-ic"><i class="fa fa-facebook"></i></a></li>
                                        <li><a class="icons-sm tw-ic"><i class="fa fa-twitter"></i></a></li>
                                        <li><a class="icons-sm gplus-ic"><i class="fa fa-google-plus"></i></a></li>
                                        <li><a class="icons-sm li-ic"><i class="fa fa-linkedin"></i></a></li>
                                    </ul>
                                    <!--Triggering button-->


                                </div>
                            </a>
                            <!--/.Back Side-->

                        </div>
                    </div>
                </a>
            </div>
            <div class="col-md-3 md-1">
                <!--Rotating card-->
                <a class="rotate-btn" data-card="card-4">
                    <div class="card-wrapper2">
                        <div id="card-4" class="card-rotating effect__click">
                            <!--Front Side-->
                            <div class="face front">

                                <div class="card-block">
                                    <br/>
                                    <h2>বি আই টি এম শিক্ষার্থী</h2>
                                    {{--<p>শিক্ষার্থী</p>--}}
                                    {{--<!--Triggering button-->--}}
                                    {{--<p>--}}
                                    {{--অধ্যয়নরত সকল ছাত্র ছাত্রীদের সর্বশেষ খবর পেতে এখানে ক্লিক করুন।--}}
                                    {{--</p>--}}

                                </div>

                            </div>
                            <!--/.Front Side-->

                            <!--Back Side-->
                            <a class="rotate-btn" data-card="card-4">
                                <div class="face back">

                                    <!--Content-->
                                    <h4>About me</h4>

                                    <hr>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Maxime quae, dolores
                                        dicta. </p>
                                    <hr>
                                    <!--Social Icons-->
                                    <ul class="inline-ul">
                                        <li><a class="icons-sm fb-ic"><i class="fa fa-facebook"></i></a></li>
                                        <li><a class="icons-sm tw-ic"><i class="fa fa-twitter"></i></a></li>
                                        <li><a class="icons-sm gplus-ic"><i class="fa fa-google-plus"></i></a></li>
                                        <li><a class="icons-sm li-ic"><i class="fa fa-linkedin"></i></a></li>
                                    </ul>
                                    <!--Triggering button-->


                                </div>
                            </a>
                            <!--/.Back Side-->

                        </div>
                    </div>
                </a>
            </div>
            <!--/.Rotating card-->
        </div>

        <div class="row">
            <!--First column-->
            <div class="col-md-3 mb-1">
                <!--Card-->
                <div class="card card-cascade cascading-admin-card">

                    <!--Card Data-->
                    <div class="admin-up">
                        <i class="fa fa-money blue darken-3"></i>
                        <div class="data">
                            <p> BITM Students</p>
                            <h3>2000</h3>
                        </div>
                    </div>
                    <!--/.Card Data-->

                    <!--Card content-->
                    <div class="card-block">
                        <div class="progress">
                            <div class="progress-bar bg-success" role="progressbar" style="width: 25%"
                                 aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                        </div>
                        <!--Text-->
                        <p class="card-text">New student (295)</p>
                    </div>
                    <!--/.Card content-->

                </div>
                <!--/.Card-->
            </div>
            <!--/First column-->

            <!--Second column-->
            <div class="col-md-3 mb-1">
                <!--Card-->
                <div class="card card-cascade cascading-admin-card">

                    <!--Card Data-->
                    <div class="admin-up">
                        <i class="fa fa-line-chart deep-purple darken-4"></i>
                        <div class="data">
                            <p> Running Courses</p>
                            <h3>8</h3>
                        </div>
                    </div>
                    <!--/.Card Data-->

                    <!--Card content-->
                    <div class="card-block">
                        <div class="progress">
                            <div class="progress-bar bg-danger" role="progressbar" style="width: 25%" aria-valuenow="25"
                                 aria-valuemin="0" aria-valuemax="100"></div>
                        </div>
                        <!--Text-->
                        <p class="card-text">2 course is coming soon..</p>
                    </div>
                    <!--/.Card content-->

                </div>
                <!--/.Card-->
            </div>
            <!--/Second column-->

            <!--Third column-->
            <div class="col-md-3 mb-1">
                <!--Card-->
                <div class="card card-cascade cascading-admin-card">

                    <!--Card Data-->
                    <div class="admin-up">
                        <i class="fa fa-pie-chart indigo"></i>
                        <div class="data">
                            <p> Students Result</p>
                            <h3>12</h3>
                        </div>
                    </div>
                    <!--/.Card Data-->

                    <!--Card content-->
                    <div class="card-block">
                        <div class="progress">
                            <div class="progress-bar bg-success" role="progressbar" style="width: 75%"
                                 aria-valuenow="75" aria-valuemin="0" aria-valuemax="100"></div>
                        </div>
                        <!--Text-->
                        <p class="card-text">2 result is under process..</p>
                    </div>
                    <!--/.Card content-->

                </div>
                <!--/.Card-->
            </div>
            <!--/Third column-->

            <!--Fourth column-->
            <div class="col-md-3 mb-1">
                <!--Card-->
                <div class="card card-cascade cascading-admin-card">

                    <!--Card Data-->
                    <div class="admin-up">
                        <i class="fa fa-bar-chart purple"></i>
                        <div class="data">
                            <p>Upcoming Paid Course</p>
                            <h3>3+</h3>
                        </div>
                    </div>
                    <!--/.Card Data-->

                    <!--Card content-->
                    <div class="card-block">
                        <div class="progress">
                            <div class="progress-bar bg-success" role="progressbar" style="width: 25%"
                                 aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                        </div>
                        <!--Text-->
                        <p class="card-text">Start soon......</p>
                    </div>
                    <!--/.Card content-->

                </div>
                <!--/.Card-->
            </div>
            <!--/Fourth column-->

        </div>
        <!--/First row-->

@endsection
@section('notification')
    <section class="section section-intro">

        <!--Second row-->
        <div class="row mb-3">

            <!--First column-->
            <div class="col-md-4 mb-1">

                <!--Card-->
                <div class="card card-cascade narrower">
                    <div class="admin-panel info-admin-panel">
                        <!--Card image-->
                        <div class="view">
                            <h5>Top 5 Student From Last Quarter</h5>
                        </div>
                        <!--/Card image-->

                        <!--Card content-->
                        <div class="card-block">

                            <div class="list-group">
                                <a href="#" class="list-group-item">Al Amin Hossain (PHP) <i class="fa fa-eye ml-auto"
                                                                                             data-toggle="tooltip"
                                                                                             data-placement="top"
                                                                                             title="Click to fix"></i></a>
                                <a href="#" class="list-group-item">Akash Rahman ( Laravel )<i
                                            class="fa fa-wrench ml-auto"
                                            data-toggle="tooltip"
                                            data-placement="top"
                                            title="Click to fix"></i></a>
                                <a href="#" class="list-group-item">Williamson (Web Design) <i
                                            class="fa fa-wrench ml-auto"
                                            data-toggle="tooltip"
                                            data-placement="top"
                                            title="Click to fix"></i></a>
                                <a href="#" class="list-group-item">Abdus Salam (Graphics Design)<i
                                            class="fa fa-wrench ml-auto"
                                            data-toggle="tooltip"
                                            data-placement="top"
                                            title="Click to fix"></i></a>
                                <a href="#" class="list-group-item">Otto Roth (Cyber Security) <i
                                            class="fa fa-wrench ml-auto"
                                            data-toggle="tooltip"
                                            data-placement="top"
                                            title="Click to fix"></i></a>
                            </div>

                        </div>
                        <!--/.Card content-->

                    </div>
                </div>
                <!--/.Card-->
            </div>
            <!--/First column-->

            <!--Second column-->
            <div class="col-md-4 mb-1">

                <!--Card-->
                <div class="card card-cascade narrower">
                    <div class="admin-panel info-admin-panel">
                        <!--Card image-->
                        <div class="view">
                            <h5>Required Software List for Lab</h5>
                        </div>
                        <!--/Card image-->

                        <!--Card content-->
                        <div class="card-block">

                            <div class="list-group">
                                <a href="#" class="list-group-item">Responsive Website Design <i
                                            class="fa fa-eye ml-auto"
                                            data-toggle="tooltip"
                                            data-placement="top"
                                            title="Click to fix"></i>
                                </a>

                                <a href="#" class="list-group-item">Web Application Development-PHP<i
                                            class="fa fa-eye ml-auto"
                                            data-toggle="tooltip"
                                            data-placement="top"
                                            title="Click to fix"></i>
                                </a>
                                <a href="#" class="list-group-item">Application Development - Laravel<i
                                            class="fa fa-eye ml-auto"
                                            data-toggle="tooltip"
                                            data-placement="top"
                                            title="Click to fix"></i>
                                </a>
                                <a href="#" class="list-group-item">Application Development - CakePHP<i
                                            class="fa fa-eye ml-auto"
                                            data-toggle="tooltip"
                                            data-placement="top"
                                            title="Click to fix"></i>
                                </a>
                                <a href="#" class="list-group-item">Graphics Design<i
                                            class="fa fa-eye ml-auto"
                                            data-toggle="tooltip"
                                            data-placement="top"
                                            title="Click to fix"></i>
                                </a>

                            </div>

                        </div>
                        <!--/.Card content-->

                    </div>
                </div>
                <!--/.Card-->
            </div>
            <!--/Second column-->

            <!--Third column-->
            <div class="col-md-4 mb-1">
                <!--Card-->
                <div class="card card-cascade narrower">
                    <div class="admin-panel info-admin-panel">
                        <!--Card image-->
                        <div class="view">
                            <h5>Necessary Documents</h5>
                        </div>
                        <!--/Card image-->

                        <!--Card content-->
                        <div class="card-block">

                            <div class="list-group">
                                <a href="#" class="list-group-item">Course outline for upcoming batch <i
                                            class="fa fa-eye ml-auto"
                                            data-toggle="tooltip"
                                            data-placement="top"
                                            title="Click to fix"></i>
                                </a>
                                <a href="#" class="list-group-item">Result of recently completed batches <i
                                            class="fa fa-eye ml-auto"
                                            data-toggle="tooltip"
                                            data-placement="top"
                                            title="Click to fix"></i>
                                </a>
                                <a href="#" class="list-group-item">Resume of Trainer <i class="fa fa-eye ml-auto"
                                                                                         data-toggle="tooltip"
                                                                                         data-placement="top"
                                                                                         title="Click to fix"></i>
                                </a>
                                <a href="#" class="list-group-item">Resume of Recommandaed Students <i
                                            class="fa fa-eye ml-auto"
                                            data-toggle="tooltip"
                                            data-placement="top"
                                            title="Click to fix"></i>
                                </a>
                                <a href="#" class="list-group-item">Upcoming best students (all batch) <i
                                            class="fa fa-eye ml-auto"
                                            data-toggle="tooltip"
                                            data-placement="top"
                                            title="Click to fix"></i>
                                </a>

                            </div>

                        </div>
                        <!--/.Card content-->

                    </div>
                </div>
                <!--/.Card-->
            </div>
            <!--/Third column-->

        </div>
        <!--/Second row-->

    </section>
@endsection
