@extends('admin.layouts.master')
@section('main_title', 'Adding New Lab')
@section('bread_crumb')
    <li class="breadcrumb-item"><a href="{{ url('admin/lab') }}">Labs</a> </li>
    <li class="breadcrumb-item active">Adding New Lab</li>
@endsection
@section('content')

    {!! Form::open(['url' => 'admin/lab', 'method' => 'POST', 'files' => true ]) !!}
    @include('admin.lab.form')
    {!! Form::close() !!}
@endsection

