@extends('admin.layouts.master')
@section('main_title', 'Edit Lab information')
@section('bread_crumb')
    <li class="breadcrumb-item"><a href="{{ url('admin/lab') }}">Labs</a> </li>
    <li class="breadcrumb-item active">Editing lab information</li>
@endsection
@section('content')
    {!! Form::open( ['url' => 'admin/lab/'.$lab->id, 'method' => 'PATCH', 'files' => true ]) !!}
    {{--{{ dd($lab) }}--}}
    @include('admin.lab.form')
    {!! Form::close() !!}
@endsection

