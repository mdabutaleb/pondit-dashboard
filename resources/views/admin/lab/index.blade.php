@extends('admin.layouts.master')
@section('main_title', 'List of Company')
@section('bread_crumb')
    <li class="breadcrumb-item active"><a href="{{ url('admin/lab') }}">Lab</a></li>
    <li class="breadcrumb-item active">All lab information</li>
@endsection
@section('content')
    <div class="row">
        <div class="col-md-2">

        </div>
        <div class="col-md-8">
            @if(Session::has('flash_message'))

                <button type="button" class="btn btn-success">
                    {!! session('flash_message') !!}
                </button>
            @endif
            <table class="table table-hover">
                <thead>
                <tr>
                    <th>Company Name</th>
                    <th>Lab</th>
                    <th>Seat Capacity</th>
                    <th>AC Status</th>
                    <th>Projector?</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>

                @foreach($labs as $lab)
                    <tr>
                        <td>{{ $lab->TrainingPartner->title }}</td>
                        <td>{{ $lab->title }}</td>
                        <td>{{ $lab->capacity }}</td>
                        <td>{{ $lab->is_ac }}</td>
                        <td>{{ $lab->is_projector }}</td>
                        <td>
                            {{--<a class="blue-text"><i class="fa fa-user"></i></a>--}}
                            <a href="{{ url('admin/lab', [$lab->id, 'edit']) }}" class="teal-text"><i
                                        class="fa fa-pencil"></i></a>
                            {!! Form::open(['method' => 'DELETE', 'url' => 'admin/lab/'.$lab->id,  'class' =>'red-text']) !!}
                            {!! Form::button( '<i class="fa fa-times"></i>', [
                            'type' => 'submit',
                            'title' => 'Delete Lab',
                            'onclick'=>'return confirm("Are you sure you want to delete ?")'] ) !!}
                            {!! Form::close() !!}


                        </td>
                    </tr>
                @endforeach

                </tbody>
            </table>
            <ul class="pagination pg-blue">
                {{ $labs->links() }}
            </ul>

        </div>

    </div>
@endsection

