<br/>
<div class="row">
    <div class="col-md-2">

    </div>
    <div class="col-md-5">
        @if(Session::has('flash_message'))
            <a href="{{ url('admin/lab') }}">
                <button type="button" class="btn btn-success">
                    {!! session('flash_message') !!}
                    Click Here For List of Lab
                </button>
            </a>
        @endif


        <div class="md-form">
            {{--@if(isset($lab))--}}
                {{--{!! Form::select('company_id', $companies,  [$lab->company_id, $companies], ['required', 'class'=> 'mdb-select colorful-select dropdown-primary', 'placeholder' => 'Select Company']) !!}--}}
            {{--@else--}}
                {{--{!! Form::select('company_id', $companies, null , ['required', 'class'=> 'mdb-select colorful-select dropdown-primary', 'placeholder' => 'Select Company']) !!}--}}
            {{--@endif--}}
            {!! Form::select('training_partner_id', $TrainingPartners,  isset($lab)?[$lab->training_partner_id, $TrainingPartners]:null, ['required', 'class'=> 'mdb-select colorful-select dropdown-primary', 'placeholder' => 'Select Training Partner']) !!}

        </div>

        <div class="md-form">
            {!! Form::label('title', 'Lab Name/No')!!}
            {!! Form::text('title', isset($lab->title) ? $lab->title : null, ['class' => 'form-control'])!!}

        </div>
        <div class="md-form">
            {!! Form::label('description', 'Description')!!}
            {!! Form::textarea('description', isset($lab->description) ? $lab->description : null, ['class' => 'md-textarea'])!!}

        </div>

    </div>
    <div class="col-md-4">
        <div class="md-form">
            {!! Form::label('capacity', 'Seat Capacity')!!}
            {!! Form::text('capacity', isset($lab->capacity) ? $lab->capacity : null)!!}

        </div>
        <div class="row">

            <div class="col-md-5">
                <p> AC Available ? </p>
                <fieldset class="form-group">
                {!! Form::radio('is_ac', 'YES', isset($lab)&& ($lab->is_ac =='YES') ? true : null, ['id' =>'is_ac']) !!}
                {!! Form::label('is_ac', 'YES') !!}

                </fieldset>
                <fieldset class="form-group">
                {!! Form::radio('is_ac', 'No', isset($lab)&& ($lab->is_ac =='NO') ? true : null, ['id' =>'is_ac2']) !!}
                {!! Form::label('is_ac2', 'NO') !!}
                </fieldset>


            </div>


            <div class="col-md-7">
                <p> Projector Available ? </p>

                <fieldset class="form-group">
                {!! Form::radio('is_projector', 'YES', isset($lab) && ($lab->is_projector =='YES') ? true : null, ['id' =>'projector1']) !!}
                {!! Form::label('projector1', 'YES') !!}

                </fieldset>
                <fieldset class="form-group">
                {!! Form::radio('is_projector', 'No', isset($lab) && ($lab->is_projector =='NO') ? true : null, ['id' =>'projector2']) !!}
                {!! Form::label('projector2', 'NO') !!}
                </fieldset>
            </div>

        </div>
        {!! Form::submit('     Save Lab Information', ['class' => 'btn btn-outline-default waves-effect']) !!}

    </div>

</div>