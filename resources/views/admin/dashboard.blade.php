@extends('admin.layouts.master')
@section('main_title', 'Dashboard')
@section('bread_crumb')
    {{--<li class="breadcrumb-item active">My location</li>--}}
@endsection
@section('statistics')
    <section class="section section-intro">
        <!--First row-->
        <div class="row">
            <!--First column-->
            <div class="col-md-3 mb-1">
                <!--Card-->
                <div class="card card-cascade cascading-admin-card">

                    <!--Card Data-->
                    <div class="admin-up">
                        <i class="fa fa-money blue darken-3"></i>
                        <div class="data">
                            <h6>বি আই টি এম শিক্ষার্থী</h6>
                            {{--<p> BITM Students</p>--}}
                            <h3>২০০০</h3>
                        </div>
                    </div>
                    <!--/.Card Data-->

                    <!--Card content-->
                    <div class="card-block">
                        <div class="progress">
                            <div class="progress-bar bg-success" role="progressbar" style="width: 25%"
                                 aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                        </div>
                        <!--Text-->
                        <p class="card-text">নতুন শিক্ষার্থী (২৯৫)</p>
                    </div>
                    <!--/.Card content-->

                </div>
                <!--/.Card-->
            </div>
            <!--/First column-->

            <!--Second column-->
            <div class="col-md-3 mb-1">
                <!--Card-->
                <div class="card card-cascade cascading-admin-card">

                    <!--Card Data-->
                    <div class="admin-up">
                        <i class="fa fa-line-chart deep-purple darken-4"></i>
                        <div class="data">
                            <h6> চলমান কোর্স</h6>
                            <h3>৮</h3>
                        </div>
                    </div>
                    <!--/.Card Data-->

                    <!--Card content-->
                    <div class="card-block">
                        <div class="progress">
                            <div class="progress-bar bg-danger" role="progressbar" style="width: 25%" aria-valuenow="25"
                                 aria-valuemin="0" aria-valuemax="100"></div>
                        </div>
                        <!--Text-->
                        <p class="card-text">২ টি কোর্স শীঘ্রই শুরু হবে.</p>
                    </div>
                    <!--/.Card content-->

                </div>
                <!--/.Card-->
            </div>
            <!--/Second column-->

            <!--Third column-->
            <div class="col-md-3 mb-1">
                <!--Card-->
                <div class="card card-cascade cascading-admin-card">

                    <!--Card Data-->
                    <div class="admin-up">
                        <i class="fa fa-pie-chart indigo"></i>
                        <div class="data">
                            <h6> শিক্ষার্থীদের ফলাফল</h6>
                            <h3>১২</h3>
                        </div>
                    </div>
                    <!--/.Card Data-->

                    <!--Card content-->
                    <div class="card-block">
                        <div class="progress">
                            <div class="progress-bar bg-success" role="progressbar" style="width: 75%"
                                 aria-valuenow="75" aria-valuemin="0" aria-valuemax="100"></div>
                        </div>
                        <!--Text-->
                        <p class="card-text"> ২টি ফলাফল প্রক্রিয়াধীন আছে</p>
                    </div>
                    <!--/.Card content-->

                </div>
                <!--/.Card-->
            </div>
            <!--/Third column-->

            <!--Fourth column-->
            <div class="col-md-3 mb-1">
                <!--Card-->
                <div class="card card-cascade cascading-admin-card">

                    <!--Card Data-->
                    <div class="admin-up">
                        <i class="fa fa-bar-chart purple"></i>
                        <div class="data">
                            <h6>শীঘ্রই শুরু হবে</h6>
                            <h3>৩+</h3>
                        </div>
                    </div>
                    <!--/.Card Data-->

                    <!--Card content-->
                    <div class="card-block">
                        <div class="progress">
                            <div class="progress-bar bg-success" role="progressbar" style="width: 25%"
                                 aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                        </div>
                        <!--Text-->
                        <p class="card-text">শীঘ্রই শুরু হবে</p>
                    </div>
                    <!--/.Card content-->

                </div>
                <!--/.Card-->
            </div>
            <!--/Fourth column-->

        </div>
        <!--/First row-->
    </section>
@endsection
@section('notification')
    <section class="section section-intro">
        <!--Second row-->
        <div class="row mb-3">
            <!--First column-->
            <div class="col-md-4 mb-1">
                <!--Card-->
                <div class="card card-cascade narrower">
                    <div class="admin-panel info-admin-panel">
                        <!--Card image-->
                        <div class="view">
                            <h5>Top 5 Student From Last Quarter</h5>
                        </div>
                        <!--/Card image-->

                        <!--Card content-->
                        <div class="card-block">

                            <div class="list-group">
                                <a href="#" class="list-group-item">Al Amin Hossain (PHP) <i class="fa fa-eye ml-auto"
                                                                                             data-toggle="tooltip"
                                                                                             data-placement="top"
                                                                                             title="Click to fix"></i></a>
                                <a href="#" class="list-group-item">Akash Rahman ( Laravel )<i
                                            class="fa fa-wrench ml-auto"
                                            data-toggle="tooltip"
                                            data-placement="top"
                                            title="Click to fix"></i></a>
                                <a href="#" class="list-group-item">Williamson (Web Design) <i
                                            class="fa fa-wrench ml-auto"
                                            data-toggle="tooltip"
                                            data-placement="top"
                                            title="Click to fix"></i></a>
                                <a href="#" class="list-group-item">Abdus Salam (Graphics Design)<i
                                            class="fa fa-wrench ml-auto"
                                            data-toggle="tooltip"
                                            data-placement="top"
                                            title="Click to fix"></i></a>
                                <a href="#" class="list-group-item">Otto Roth (Cyber Security) <i
                                            class="fa fa-wrench ml-auto"
                                            data-toggle="tooltip"
                                            data-placement="top"
                                            title="Click to fix"></i></a>
                            </div>

                        </div>
                        <!--/.Card content-->

                    </div>
                </div>
                <!--/.Card-->
            </div>
            <!--/First column-->

            <!--Second column-->
            <div class="col-md-4 mb-1">

                <!--Card-->
                <div class="card card-cascade narrower">
                    <div class="admin-panel info-admin-panel">
                        <!--Card image-->
                        <div class="view">
                            <h5>Required Software List for Lab</h5>
                        </div>
                        <!--/Card image-->

                        <!--Card content-->
                        <div class="card-block">

                            <div class="list-group">
                                <a href="#" class="list-group-item">Responsive Website Design <i
                                            class="fa fa-eye ml-auto"
                                            data-toggle="tooltip"
                                            data-placement="top"
                                            title="Click to fix"></i>
                                </a>

                                <a href="#" class="list-group-item">Web Application Development-PHP<i
                                            class="fa fa-eye ml-auto"
                                            data-toggle="tooltip"
                                            data-placement="top"
                                            title="Click to fix"></i>
                                </a>
                                <a href="#" class="list-group-item">Application Development - Laravel<i
                                            class="fa fa-eye ml-auto"
                                            data-toggle="tooltip"
                                            data-placement="top"
                                            title="Click to fix"></i>
                                </a>
                                <a href="#" class="list-group-item">Application Development - CakePHP<i
                                            class="fa fa-eye ml-auto"
                                            data-toggle="tooltip"
                                            data-placement="top"
                                            title="Click to fix"></i>
                                </a>
                                <a href="#" class="list-group-item">Graphics Design<i
                                            class="fa fa-eye ml-auto"
                                            data-toggle="tooltip"
                                            data-placement="top"
                                            title="Click to fix"></i>
                                </a>

                            </div>

                        </div>
                        <!--/.Card content-->

                    </div>
                </div>
                <!--/.Card-->
            </div>
            <!--/Second column-->

            <!--Third column-->
            <div class="col-md-4 mb-1">
                <!--Card-->
                <div class="card card-cascade narrower">
                    <div class="admin-panel info-admin-panel">
                        <!--Card image-->
                        <div class="view">
                            <h5>Necessary Documents</h5>
                        </div>
                        <!--/Card image-->

                        <!--Card content-->
                        <div class="card-block">

                            <div class="list-group">
                                <a href="#" class="list-group-item">Course outline for upcoming batch <i
                                            class="fa fa-eye ml-auto"
                                            data-toggle="tooltip"
                                            data-placement="top"
                                            title="Click to fix"></i>
                                </a>
                                <a href="#" class="list-group-item">Result of recently completed batches <i
                                            class="fa fa-eye ml-auto"
                                            data-toggle="tooltip"
                                            data-placement="top"
                                            title="Click to fix"></i>
                                </a>
                                <a href="#" class="list-group-item">Resume of Trainer <i class="fa fa-eye ml-auto"
                                                                                         data-toggle="tooltip"
                                                                                         data-placement="top"
                                                                                         title="Click to fix"></i>
                                </a>
                                <a href="#" class="list-group-item">Resume of Recommandaed Students <i
                                            class="fa fa-eye ml-auto"
                                            data-toggle="tooltip"
                                            data-placement="top"
                                            title="Click to fix"></i>
                                </a>
                                <a href="#" class="list-group-item">Upcoming best students (all batch) <i
                                            class="fa fa-eye ml-auto"
                                            data-toggle="tooltip"
                                            data-placement="top"
                                            title="Click to fix"></i>
                                </a>

                            </div>

                        </div>
                        <!--/.Card content-->

                    </div>
                </div>
                <!--/.Card-->
            </div>
            <!--/Third column-->

        </div>
        <!--/Second row-->

    </section>
@endsection