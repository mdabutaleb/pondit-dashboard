@extends('admin.layouts.master')
@section('main_title', 'Adding New course')
@section('bread_crumb')
    <li class="breadcrumb-item"><a href="{{ url('admin/course') }}">Course</a> </li>
    <li class="breadcrumb-item active"><a href="">Adding New course</a> </li>

@endsection
@section('content')

    {!! Form::open(['url' => 'admin/course', 'method' => 'POST', 'files' => true ]) !!}
    @include('admin.course.form')
    {!! Form::close() !!}
@endsection

