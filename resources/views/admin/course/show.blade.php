@extends('admin.layouts.master')
@section('main_title')
    {{ 'course -'. $course->title }}
@endsection

@section('bread_crumb')
    <li class="breadcrumb-item"><a href="{{ url('admin/course') }}">Course</a></li>
    <li class="breadcrumb-item active"><a href="{{ url('admin/course/create') }}">Add New course</a></li>
    <li class="breadcrumb-item "><a href="{{ url('admin/course') }}">Back to list</a></li>
@endsection
{{--<style>--}}
{{--.table th {--}}
{{--text-align: center;--}}
{{--}--}}
{{--</style>--}}
@section('content')
    <div class="row">
        <div class="col-md-12">
            @if(Session::has('flash_message'))

                <button type="button" class="btn btn-success">
                    {!! session('flash_message') !!}
                </button>
            @endif
            <table class="table table-responsive">
                <thead>
                <tr>
                    <th>Course Name</th>
                    <th>Training Partner</th>
                    <th>Lead Trainer</th>
                    <th>Asst. Trainer</th>
                    <th>Outline</th>
                    <th>Start Date</th>
                    <th>End Date</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>


                <tr>
                    <td>{{ $course->title }}</td>
                    <td>{{ $course->training_partner->title }}</td>
                    <td><img src="{{ asset($course->leadTrainer->img) }}" width="110px"></td>
                    <td><img src="{{ asset($course->asstTrainer->img) }}" width="110px"></td>
                    {{--                        <td>{{ $course->asst_trainer }}</td>--}}
                    <td><a href="{{ asset($course->outline) }}" target="_blank"> Download</a></td>
                    <td>{{ $course->start_date }}</td>
                    <td>{{ $course->end_date }}</td>
                    <td>
                        <div class="btn-group">
                            {{--<a class="blue-text"><i class="fa fa-user"></i></a>--}}

                            <a href="{{ url('admin/course', [$course->id, 'edit']) }}" class="teal-text"><i
                                        class="fa fa-pencil"></i> </a>
                            &nbsp;&nbsp;
                            {!! Form::open(['method' => 'DELETE', 'url' => 'admin/course/'.$course->id]) !!}
                            {!! Form::button( '<i class="fa fa-times"></i>', [
                            'type' =>'submit',
                            'title' => 'Delete Tag',

                            'onclick'=>'return confirm("Are you sure you want to delete ?")'] ) !!}
                            {!! Form::close() !!}
                        </div>

                    </td>
                </tr>

                </tbody>
            </table>

        </div>

    </div>
@endsection

