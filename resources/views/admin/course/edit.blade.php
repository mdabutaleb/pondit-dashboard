@extends('admin.layouts.master')
@section('main_title', 'Edit Lab information')
@section('bread_crumb')
    <li class="breadcrumb-item"><a href="{{ url('admin/course') }}">Labs</a></li>
    <li class="breadcrumb-item active">Editing course information</li>
@endsection
@section('content')
    {!! Form::model($course, ['url' => 'admin/course/'.$course->id, 'method' => 'PATCH', 'files' => true ]) !!}
    {{--{{ dd($lab) }}--}}
    @include('admin.course.form')
    {!! Form::close() !!}
@endsection

