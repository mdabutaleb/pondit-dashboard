<div class="row">
    <div class="col-md-2">

    </div>

    <div class="col-md-6">
        @if(Session::has('flash_message'))
            <a href="{{ url('admin/course') }}">
                <button type="button" class="btn btn-success">
                    {!! session('flash_message') !!}
                    Click Here For List of Courses
                </button>
            </a>
        @endif
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <div class="row">
            <div class="col-md-12">
                <div class="md-form">
                    {!! Form::label('title', 'Course Name')!!}
                    {!! Form::text('title', null, ['class' => 'form-control'])!!}
                </div>
            </div>
            <div class="col-md-6">

                <div class="md-form">
                    {!! Form::select('training_partner_id', $TrainingPartners,  null, ['class'=> 'mdb-select colorful-select dropdown-primary', 'placeholder' => 'Training Partner']) !!}
                </div>


                <div class="md-form">
                    {!! Form::select('lead_trainer', $LeadTrainers,  null, ['class'=> 'mdb-select colorful-select dropdown-primary', 'placeholder' => 'Lead Trainer']) !!}
                </div>
                <div class="md-form">
                    {!! Form::select('asst_trainer', $AssistantTrainer,  null, [ 'class'=> 'mdb-select colorful-select dropdown-primary', 'placeholder' => 'Assistant Trainer']) !!}
                </div>


            </div>
            <div class="col-md-6">
                <div class="md-form">
                    {!! Form::text('start_date', null, ['id'=>'start_date', 'class' => 'form-control datepicker' ]) !!}
                    {!! Form::label('start_date', 'Start Date', ['id' => 'start_date'])!!}
                </div>
                <div class="md-form">
                    {!! Form::text('end_date', null, ['id'=>'end_date', 'class' => 'form-control datepicker' ]) !!}
                    {!! Form::label('end_date', 'End Date', ['id' => 'end_date'])!!}
                </div>


                <div class="md-form">
                    {!! Form::label('course_length', 'Course Length')!!}
                    {!! Form::text('course_length', null, ['class' => 'form-control'])!!}

                </div>

            </div>
            <div class="col-md-12">
                <div class="md-form">
                    {!! Form::textarea('description', null, ['class' => 'md-textarea'])!!}
                    {!! Form::label('description', 'Somethig about course' )!!}
                </div>
            </div>


        </div>
    </div>


    <div class="col-md-2">
        <br/>
        @php
            $img = isset($trainer->img) ? $trainer->img : 'img/default.png';
        @endphp


        <div class="file-field">
            {!! Form::file('outline') !!}
            <div class="file-path-wrapper">
                <input class="file-path validate" type="text" placeholder=" Upload Course outline   ">
            </div>
        </div>
        {!! Form::submit('       Save Course  Info  ', ['class' => 'btn btn-outline-default waves-effect']) !!}
    </div>
</div>

<script>
    $('.datepicker').pickadate();
    formatSubmit: 'yyyy/mm/dd';
</script>