@extends('admin.layouts.master')
@section('main_title', 'List of Course')
@section('bread_crumb')
    <li class="breadcrumb-item active"><a href="{{ url('admin/course') }}">Course</a></li>
    <li class="breadcrumb-item active"><a href="{{ url('admin/course/create') }}">Add New course</a></li>
@endsection
{{--<style>--}}
{{--.table th {--}}
{{--text-align: center;--}}
{{--}--}}
{{--</style>--}}
@section('content')
    <div class="row">
        <div class="col-md-1">

        </div>
        <div class="col-md-10">
            @if(Session::has('flash_message'))

                <button type="button" class="btn btn-success">
                    {!! session('flash_message') !!}
                </button>
            @endif
            <table class="table">
                <thead>
                <tr>
                    <th>Course Name</th>
                    <th>Training Partner</th>
                    <th>Lead Trainer</th>
                    <th>Asst. Trainer</th>
                    <th>Outline</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>

                @foreach($courses as $course)

                    <tr>
                        <td>{{ $course->title }}</td>
                        <td>{{ $course->training_partner->title }}</td>
                        <td>{{ $course->leadTrainer->title }}</td>
                        <td>{{ $course->asstTrainer->title }}</td>
                        {{--                        <td>{{ $course->asst_trainer }}</td>--}}
                        <td><a href="{{ asset($course->outline) }}" target="_blank"> Download</a></td>
                        <td>
                            <div class="btn-group">
                                {{--<a class="blue-text"><i class="fa fa-user"></i></a>--}}

                                <a href="{{ url('admin/course', [$course->id]) }}" class="teal-text"><i class="fa fa-eye"></i> </a>

                                <a href="{{ url('admin/course', [$course->id, 'edit']) }}" class="teal-text"><i class="fa fa-pencil"></i> </a>
                                &nbsp;&nbsp;
                                {!! Form::open(['method' => 'DELETE', 'url' => 'admin/course/'.$course->id]) !!}
                                {!! Form::button( '<i class="fa fa-times"></i>', [
                                'type' =>'submit',
                                'title' => 'Delete Tag',

                                'onclick'=>'return confirm("Are you sure you want to delete ?")'] ) !!}
                                {!! Form::close() !!}
                            </div>

                        </td>
                    </tr>
                @endforeach

                </tbody>
            </table>
            <ul class="pagination pg-blue">
                {{ $courses->links() }}
            </ul>

        </div>

    </div>
@endsection

