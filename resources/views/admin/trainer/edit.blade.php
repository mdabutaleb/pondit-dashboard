@extends('admin.layouts.master')
@section('main_title', 'Editing Trainer')
@section('bread_crumb')
    <li class="breadcrumb-item"><a href="{{ url('/admin/trainer') }}"> Trainer</a></li>
    <li class="breadcrumb-item active">Editing Trainer Information</li>
@endsection
@section('content')

    {!! Form::model($trainer, ['url' => 'admin/trainer/'.$trainer->id, 'method' => 'PATCH', 'files' => true ]) !!}
    @include('admin.trainer.form')
    {!! Form::close() !!}
@endsection

