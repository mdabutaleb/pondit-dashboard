<script src="{{ asset('custom/image-preview.js')}}"></script>

<div class="row">
    <div class="col-md-2">

    </div>

    <div class="col-md-6">
        @if(Session::has('flash_message'))
            <a href="{{ url('admin/trainer') }}">
                <button type="button" class="btn btn-success">
                    {!! session('flash_message') !!}
                    Click Here For List of Trainer
                </button>
            </a>
        @endif
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <div class="row">

            <div class="col-md-6">
                <div class="md-form">
                    {!! Form::label('title', 'Trainer Name')!!}
                    {!! Form::text('title', null, ['class' => 'form-control'])!!}
                </div>

                <div class="md-form">
                    {{--                    {!! Form::label('designation', 'Designation')!!}--}}
                    {{--{!! Form::text('designation', null, ['class' => 'form-control'])!!}--}}
                    {!! Form::select('designation', $designations,  null, ['required', 'class'=> 'mdb-select colorful-select dropdown-primary', 'placeholder' => 'Trainer Designation']) !!}

                </div>


                <div class="md-form">
                    {!! Form::label('mobile', 'Mobile NO')!!}
                    {!! Form::text('mobile', null, ['class' => 'form-control'])!!}

                </div>


            </div>
            <div class="col-md-6">
                <div class="md-form">
                    {!! Form::label('email', 'Email Address')!!}
                    {!! Form::text('email', null, ['class' => 'form-control'])!!}

                </div>
                <div class="md-form">
                    {!! Form::label('web', 'Website URL')!!}
                    {!! Form::text('web', null, ['class' => 'form-control'])!!}

                </div>
                <div class="md-form">
                    {!! Form::label('git', 'Gitlab/Github/Bitbucket')!!}
                    {!! Form::text('git', null, ['class' => 'form-control'])!!}

                </div>

            </div>
            <div class="col-md-12">
                <div class="md-form">
                    {!! Form::textarea('description', null, ['class' => 'md-textarea'])!!}
                    {!! Form::label('description', 'Trainer Bio' )!!}
                </div>
            </div>


        </div>
    </div>


    <div class="col-md-2">
        <br/>
        @php
            $img = isset($trainer->img) ? $trainer->img : 'img/default.png';
        @endphp

        <div class="file-field">
            <img src="{{ asset($img) }}" id="profile" class="img-responsive" width="200"
                 height="230"/>
            {!! Form::file('img', ['onchange' => 'ProfleRewview(this)']) !!}
            <div class="file-path-wrapper">
                <input class="file-path validate" type="text" placeholder="          Trainer Photo">
            </div>
        </div>

        <div class="file-field">
            {!! Form::file('cv') !!}
            <div class="file-path-wrapper">
                <input class="file-path validate" type="text" placeholder="             Upload CV   ">
            </div>
        </div>
        {!! Form::submit('       Save Trainer  Info  ', ['class' => 'btn btn-outline-default waves-effect']) !!}
    </div>
</div>