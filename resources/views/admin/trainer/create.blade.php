@extends('admin.layouts.master')
@section('main_title', 'Adding New Trainer')
@section('bread_crumb')
    <li class="breadcrumb-item"><a href="{{ url('/admin/trainer') }}"> Trainer</a></li>
    <li class="breadcrumb-item active">Adding New Trainer</li>
@endsection
@section('content')

    {!! Form::open(['url' => 'admin/trainer', 'method' => 'POST', 'files' => true ]) !!}
    @include('admin.trainer.form')
    {!! Form::close() !!}
@endsection

