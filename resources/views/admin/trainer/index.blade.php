@extends('admin.layouts.master')
@section('main_title', 'List of Company')
@section('bread_crumb')
    <li class="breadcrumb-item active"><a href="{{ url('admin/trainer') }}">Trainer</a></li>
    <li class="breadcrumb-item active"><a href="{{ url('admin/trainer/create') }}">Add New</a></li>
@endsection
{{--<style>--}}
    {{--.table th {--}}
        {{--text-align: center;--}}
    {{--}--}}
{{--</style>--}}
@section('content')
    <div class="row">
        <div class="col-md-1">

        </div>
        <div class="col-md-10">
            @if(Session::has('flash_message'))

                <button type="button" class="btn btn-success">
                    {!! session('flash_message') !!}
                </button>
            @endif
            <table class="table">
                <thead>
                <tr>
                    <th>Trainer Name</th>
                    <th>Lead Trainer?</th>
                    <th>Image</th>
                    <th>Email</th>
                    <th>Phone Number</th>
                    <th>Download CV</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>

                @foreach($trainers as $trainer)
                    <tr>
                        <td>{{ $trainer->title }}</td>
                        <td>{{ ($trainer->designation==1) ? 'Yes' :'No' }}</td>
                        <td><img src="{{ asset($trainer->img) }}" width="60" height="80"></td>
                        <td>{{ $trainer->email }}</td>
                        <td>{{ $trainer->mobile }}</td>
                        <td><a href="{{ asset($trainer->cv) }}" target="_blank"> Resume</a></td>
                        <td>
                            {{--<a class="blue-text"><i class="fa fa-user"></i></a>--}}
                            <a href="{{ url('admin/trainer', [$trainer->id, 'edit']) }}" class="teal-text"><i
                                        class="fa fa-pencil"></i></a>
                            {!! Form::open(['method' => 'DELETE', 'url' => 'admin/trainer/'.$trainer->id,  'class' =>'red-text']) !!}
                            {!! Form::button( '<i class="fa fa-times"></i>', [
                            'type' => 'submit',
                            'title' => 'Delete Tag',
                            'onclick'=>'return confirm("Are you sure you want to delete ?")'] ) !!}
                            {!! Form::close() !!}


                        </td>
                    </tr>
                @endforeach

                </tbody>
            </table>
            <ul class="pagination pg-blue">
                {{ $trainers->links() }}
            </ul>

        </div>

    </div>
@endsection

