@extends('admin.layouts.master')
@section('main_title', 'Adding New Company')
@section('bread_crumb')
    <li class="breadcrumb-item active"><a href="{{ url('admin/institute') }}">Institutes</a></li>
    <li class="breadcrumb-item active"><a href="{{ url('admin/institute/create') }}"> Add new Institute</a></li>
@endsection
@section('content')

    {!! Form::open(['url' => 'admin/institute', 'method' => 'POST', 'files' => true ]) !!}
    @include('admin.institute.form')
    {!! Form::close() !!}
@endsection

