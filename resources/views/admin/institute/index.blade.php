@extends('admin.layouts.master')
@section('main_title', 'List of Company')
@section('bread_crumb')
    <li class="breadcrumb-item active"><a href="{{ url('admin/institute') }}">Institutes</a></li>
    {{--<li class="breadcrumb-item active">List of Training Partner</li>--}}
    <li class="breadcrumb-item active"><a href="{{ url('admin/institute/create') }}"> Add new Institute</a></li>

@endsection
@section('content')
    <div class="row">
        <div class="col-md-2">

        </div>
        <div class="col-md-8">
            @if(Session::has('flash_message'))

                <button type="button" class="btn btn-success">
                    {!! session('flash_message') !!}
                </button>
            @endif
            <table class="table">
                <thead>
                <tr>
                    <th>Institute Name</th>
                    <th>Logo</th>
                    <th>Address</th>
                    <th>Is Active</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>

                @foreach($institutes as $institute)
                    <tr>
                        <td>{{ $institute->title }}</td>
                        <td><img src="{{ asset($institute->logo) }}" width="100" height="50"></td>
                        <td>{{ $institute->address }}</td>
                        <td>{{ $institute->display }}</td>
                        <td>
                            {{--<a class="blue-text"><i class="fa fa-user"></i></a>--}}
                            <a href="{{ url('admin/institute', [$institute->id, 'edit']) }}" class="teal-text"><i
                                        class="fa fa-pencil"></i></a>
                            {!! Form::open(['method' => 'DELETE', 'url' => 'admin/institute/'.$institute->id,  'class' =>'red-text']) !!}
                            {!! Form::button( '<i class="fa fa-times"></i>', [
                            'type' => 'submit',
                            'title' => 'Delete Tag',
                            'onclick'=>'return confirm("Are you sure you want to delete ?")'] ) !!}
                            {!! Form::close() !!}


                        </td>
                    </tr>
                @endforeach

                </tbody>
            </table>
            <ul class="pagination pg-blue">
                {{ $institutes->links() }}
            </ul>

        </div>

    </div>
@endsection

