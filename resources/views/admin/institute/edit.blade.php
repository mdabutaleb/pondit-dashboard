@extends('admin.layouts.master')
@section('main_title', 'Adding New Company')
@section('bread_crumb')
    <li class="breadcrumb-item active"><a href="{{ url('admin/institute') }}">Institutes</a></li>
    <li class="breadcrumb-item active">Edit Institute</li>
    <li class="breadcrumb-item active"><a href="{{ url('admin/institute/create') }}"> Add new Institu
@endsection
@section('content')
    {{--{{ dd($company) }}--}}

    {!! Form::open( ['url' => 'admin/institute/'. $institute->id, 'method' => 'PATCH', 'files' => true ]) !!}
    @include('admin.institute.form')
    {!! Form::close() !!}
@endsection

