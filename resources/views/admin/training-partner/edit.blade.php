@extends('admin.layouts.master')
@section('main_title', 'Edit Training Partner')
@section('bread_crumb')
    <li class="breadcrumb-item active"><a href="{{ url('admin/training-partner') }}">Training Partners</a></li>
    <li class="breadcrumb-item active">Edit Training Partner</li>
    <li class="breadcrumb-item active"><a href="{{ url('admin/training-partner/create') }}"> Add new </a></li>


@endsection
@section('content')
{{--{{ dd($partner->logo) }}--}}

    {!! Form::open(['url' => 'admin/training-partner/'. $partner->id, 'method' => 'PATCH', 'files' => true ]) !!}
    @include('admin.training-partner.form')
    {!! Form::close() !!}
@endsection

