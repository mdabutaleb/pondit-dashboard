<script src="{{ asset('custom/image-preview.js')}}"></script>

<div class="row">
    <div class="col-md-2">

    </div>
    <div class="col-md-6">
        @if(Session::has('flash_message'))
            <a href="{{ url('admin/training-partner') }}">
                <button type="button" class="btn btn-success">
                    {!! session('flash_message') !!}
                    | See Partner List
                </button>
            </a>
        @endif

        <div class="md-form">
            {!! Form::label('title', 'Training Partner Name')!!}
            {!! Form::text('title', isset($partner->title) ? $partner->title: null, ['class' => 'form-control'])!!}

        </div>
        <div class="md-form">
            {!! Form::label('description', 'Description')!!}
            {!! Form::textarea('description', isset($partner->description) ? $partner->description: null, ['class' => 'md-textarea'])!!}


        </div>


        <div class="md-form">
            {!! Form::label('address', 'Address')!!}
            {!! Form::textarea('address', isset($partner->address) ? $partner->address: null, ['class' => 'md-textarea'])!!}


        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="md-form">
                    {!! Form::label('email', 'Login Email')!!}
                    {!! Form::text('email', null, ['class' => 'form-control'])!!}

                </div>
                <div class="md-form">
                    {!! Form::label('password', 'Login Password')!!}
                    {!! Form::text('password', null, ['class' => 'form-control'])!!}

                </div>

            </div>
            <div class="col-md-3">
                <div class="md-form">
                    <p> Display ? </p>

                    <fieldset class="form-group">

                        {!! Form::radio('display', 'YES', isset($partner)&& ($partner->display =='YES') ? true : null, ['id' =>'display1']) !!}
                        {!! Form::label('display1', 'YES') !!}

                    </fieldset>
                    <fieldset class="form-group">

                        {!! Form::radio('display', 'No', isset($partner)&& ($partner->display =='NO') ? true : null, ['id' =>'display2']) !!}
                        {{--<label for="radio3">Test</label>--}}
                        {!! Form::label('display2', 'NO') !!}
                    </fieldset>
                </div>
            </div>

        </div>


    </div>
    <div class="col-md-3">
        <br/>
        @php

            $img = isset($partner->logo) ? $partner->logo : 'img/default.png';
        @endphp

        <div class="file-field">
            <img src="{{ asset($img) }}" id="logo" class="img-responsive" width="270"
                 height="150"/>
            {!! Form::file('logo', ['onchange' => 'readURL(this)']) !!}
            <div class="file-path-wrapper">
                <input class="file-path validate" type="text" placeholder="               Upload Logo">
            </div>
        </div>

        @php
            $img = isset($partner->banner) ? $partner->banner : 'img/default.png';
        @endphp

        <div class="file-field">
            <img src="{{ asset($img) }}" id="banner" class="img-responsive" width="270"
                 height="150"/>
            {!! Form::file('banner', ['onchange' => 'BannerImage(this)']) !!}
            <div class="file-path-wrapper">
                <input class="file-path validate" type="text" placeholder="        Upload Banner Image">
            </div>
        </div>
        {!! Form::submit('     Save partner Information', ['class' => 'btn btn-outline-default waves-effect']) !!}
    </div>
</div>