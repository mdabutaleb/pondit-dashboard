@extends('admin.layouts.master')
@section('main_title', 'Adding New Partner')
@section('bread_crumb')
    <li class="breadcrumb-item active"><a href="{{ url('admin/training-partner') }}">Training Partners</a></li>
    <li class="breadcrumb-item active">List of Training Partner</li>
    <li class="breadcrumb-item active"><a href="{{ url('admin/training-partner/create') }}"> Add new </a></li>

@endsection
@section('content')

    {!! Form::open(['url' => 'admin/training-partner', 'method' => 'POST', 'files' => true ]) !!}
    @include('admin.training-partner.form')
    {!! Form::close() !!}
@endsection

