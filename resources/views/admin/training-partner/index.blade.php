@extends('admin.layouts.master')
@section('main_title', 'List of Training Partner')
@section('bread_crumb')
    <li class="breadcrumb-item active"><a href="{{ url('admin/training-partner') }}">Training Partners</a></li>
    <li class="breadcrumb-item active">List of Training Partner</li>
    <li class="breadcrumb-item active"><a href="{{ url('admin/training-partner/create') }}"> Add new </a></li>


@endsection
@section('content')
    <div class="row">
        <div class="col-md-2">

        </div>
        <div class="col-md-8">
            @if(Session::has('flash_message'))

                <button type="button" class="btn btn-success">
                    {!! session('flash_message') !!}
                </button>
            @endif
            <table class="table">
                <thead>
                <tr>
                    <th>Training Partner Name</th>
                    <th>Logo</th>
                    <th>Address</th>
                    <th>Display?</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>

                @foreach($partners as $partner)
                    <tr>
                        <td>{{ $partner->title }}</td>
                        <td><img src="{{ asset($partner->logo) }}" width="100" height="50"></td>
                        <td>{{ $partner->address }}</td>
                        <td>{{ $partner->display }}</td>
                        <td>
                            {{--<a class="blue-text"><i class="fa fa-user"></i></a>--}}
                            <a href="{{ url('admin', ['training-partner', $partner->id, 'edit']) }}" class="teal-text"><i
                                        class="fa fa-pencil"></i></a>
                            {!! Form::open(['method' => 'DELETE', 'url' => 'admin/training-partner/'.$partner->id,  'class' =>'red-text']) !!}
                            {!! Form::button( '<i class="fa fa-times"></i>', [
                            'type' => 'submit',
                            'title' => 'Delete Partner',
                            'onclick'=>'return confirm("Are you sure you want to delete ?")'] ) !!}
                            {!! Form::close() !!}


                        </td>
                    </tr>
                @endforeach

                </tbody>
            </table>
            <ul class="pagination pg-blue">
                {{ $partners->links() }}
            </ul>

        </div>

    </div>
@endsection

