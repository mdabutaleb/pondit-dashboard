@extends('admin.layouts.master')
@section('main_title', 'Adding New Company')
@section('bread_crumb')
    <li class="breadcrumb-item active">Adding New Company</li>
@endsection
@section('content')

    {!! Form::open(['url' => 'admin/company', 'method' => 'POST', 'files' => true ]) !!}
    @include('admin.company.form')
    {!! Form::close() !!}
@endsection

