@extends('admin.layouts.master')
@section('main_title', 'Adding New Company')
@section('bread_crumb')
    <li class="breadcrumb-item active">Adding New Company</li>
@endsection
@section('content')
{{--{{ dd($company) }}--}}


    {!! Form::model($company, ['url' => 'admin/company/'. $company->id, 'method' => 'PATCH', 'files' => true ]) !!}
    @include('admin.company.form')
    {!! Form::close() !!}
@endsection

