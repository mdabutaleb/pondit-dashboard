<script src="{{ asset('custom/image-preview.js')}}"></script>

<div class="row">
    <div class="col-md-2">

    </div>
    <div class="col-md-6">
        @if(Session::has('flash_message'))
            <a href="{{ url('admin/company') }}">
                <button type="button" class="btn btn-success">
                    {!! session('flash_message') !!}
                    Click Here For List of Company
                </button>
            </a>
        @endif

        <div class="md-form">

            {!! Form::text('title', null, ['class' => 'form-control'])!!}
            {!! Form::label('title', 'Company Name')!!}
        </div>
        <div class="md-form">

            {!! Form::textarea('description', null, ['class' => 'md-textarea'])!!}
            {!! Form::label('description', 'Description')!!}


        </div>
        <div class="md-form">

            {!! Form::textarea('address', null, ['class' => 'md-textarea'])!!}
            {!! Form::label('address', 'Address')!!}

        </div>
        <p> Display ? </p>

        <fieldset class="form-group">
            {!! Form::radio('display', 'YES', true, ['id' =>'display1']) !!}
            {!! Form::label('display1', 'YES') !!}

        </fieldset>
        <fieldset class="form-group">
            {!! Form::radio('display', 'No', null, ['id' =>'radio3']) !!}
            {{--<label for="radio3">Test</label>--}}
            {!! Form::label('radio3', 'NO') !!}
        </fieldset>

    </div>
    <div class="col-md-3">
        <br/>
        @php
            $img = isset($company->logo) ? $company->logo : 'img/default.png';
        @endphp

        <div class="file-field">
            <img src="{{ asset($img) }}" id="logo" class="img-responsive" width="270"
                 height="150"/>
            {!! Form::file('logo', ['onchange' => 'readURL(this)']) !!}
            <div class="file-path-wrapper">
                <input class="file-path validate" type="text" placeholder="               Upload Logo">
            </div>
        </div>

        @php
            $img = isset($company->banner) ? $company->banner : 'img/default.png';
        @endphp

        <div class="file-field">
            <img src="{{ asset($img) }}" id="banner" class="img-responsive" width="270"
                 height="150"/>
            {!! Form::file('banner', ['onchange' => 'BannerImage(this)']) !!}
            <div class="file-path-wrapper">
                <input class="file-path validate" type="text" placeholder="        Upload Banner Image">
            </div>
        </div>
        {!! Form::submit('     Save Company Information', ['class' => 'btn btn-outline-default waves-effect']) !!}
    </div>
</div>