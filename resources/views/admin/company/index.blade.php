@extends('admin.layouts.master')
@section('main_title', 'List of Company')
@section('bread_crumb')
    <li class="breadcrumb-item active"><a href="{{ url('admin/company') }}">Company</a></li>
    <li class="breadcrumb-item active">All Company Name</li>
@endsection
@section('content')
    <div class="row">
        <div class="col-md-2">

        </div>
        <div class="col-md-8">
            @if(Session::has('flash_message'))

                <button type="button" class="btn btn-success">
                    {!! session('flash_message') !!}
                </button>
            @endif
            <table class="table">
                <thead>
                <tr>
                    <th>Company Name</th>
                    <th>Logo</th>
                    <th>Address</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>

                @foreach($companies as $company)
                    <tr>
                        <td>{{ $company->title }}</td>
                        <td><img src="{{ asset($company->logo) }}" width="100" height="50"></td>
                        <td>{{ $company->address }}</td>
                        <td>
                            {{--<a class="blue-text"><i class="fa fa-user"></i></a>--}}
                            <a href="{{ url('admin/company', [$company->id, 'edit']) }}" class="teal-text"><i
                                        class="fa fa-pencil"></i></a>
                            {!! Form::open(['method' => 'DELETE', 'url' => 'admin/company/'.$company->id,  'class' =>'red-text']) !!}
                            {!! Form::button( '<i class="fa fa-times"></i>', [
                            'type' => 'submit',
                            'title' => 'Delete Tag',
                            'onclick'=>'return confirm("Are you sure you want to delete ?")'] ) !!}
                            {!! Form::close() !!}


                        </td>
                    </tr>
                @endforeach

                </tbody>
            </table>
            <ul class="pagination pg-blue">
                {{ $companies->links() }}
            </ul>

        </div>

    </div>
@endsection

