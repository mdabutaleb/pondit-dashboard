<ul id="slide-out" class="side-nav fixed sn-bg-1 custom-scrollbar">
    <!-- Logo -->
    <li>
        <div class="user-box">
            <img src="{{ asset('img/logo/pondit_logo.png') }}" class="img-fluid rounded-circle">
            <p class="user text-center black-text">{{ isset(Auth::user()->name) ? Auth::user()->name : 'Name not found' }}</p>
        </div>
    </li>
    <!--/. Logo -->
    <!-- Side navigation links -->
    <li>
        <ul class="collapsible collapsible-accordion">


            <li><a class="collapsible-header waves-effect arrow-r"><i class="fa fa-industry"></i>কোম্পানি ম্যানেজমেন্ট<i
                            class="fa fa-angle-down rotate-icon"></i></a>
                <div class="collapsible-body">
                    <ul>
                        <li><a href="{{ url('admin/company/create') }}" class="waves-effect">নতুন কোম্পানি যোগ(+)করুন</a>
                        </li>
                        <li><a href="{{ url('admin/company') }}" class="waves-effect">সকল কোম্পানির তালিকা</a>
                        </li>
                    </ul>
                </div>
            </li>
            <li><a class="collapsible-header waves-effect arrow-r"><i class="fa fa-area-chart"></i>Training Partner ম্যানেজমেন্ট<i
                            class="fa fa-angle-down rotate-icon"></i></a>
                <div class="collapsible-body">
                    <ul>
                        <li>
                            <a href="{{ url('admin/training-partner/create') }}" class="waves-effect">নতুন Partner যোগ(+) করুন</a>
                        </li>
                        <li><a href="{{ url('admin/training-partner') }}" class="waves-effect">সকল Training Partner তালিকা</a>
                        </li>
                    </ul>
                </div>
            </li>
            <li><a class="collapsible-header waves-effect arrow-r"><i class="fa fa-university"></i>Institute  ম্যানেজমেন্ট<i
                            class="fa fa-angle-down rotate-icon"></i></a>
                <div class="collapsible-body">
                    <ul>
                        <li>
                            <a href="{{ url('admin/institute/create') }}" class="waves-effect">নতুন Institute যোগ(+) করুন</a>
                        </li>
                        <li><a href="{{ url('admin/institute') }}" class="waves-effect">সকল Institute তালিকা</a>
                        </li>
                    </ul>
                </div>
            </li>

            <li><a class="collapsible-header waves-effect arrow-r"><i class="fa fa-laptop"></i>Lab ম্যানেজমেন্ট<i
                            class="fa fa-angle-down rotate-icon"></i></a>
                <div class="collapsible-body">
                    <ul>
                        <li><a href="{{ url('admin/lab/create') }}" class="waves-effect">নতুন Lab যোগ(+)
                                করুন
                            </a>


                        </li>
                        <li><a href="{{ url('admin/lab') }}" class="waves-effect">সকল Lab তালিকা</a>
                        </li>
                    </ul>
                </div>
            </li>
            <li><a class="collapsible-header waves-effect arrow-r"><i class="fa fa-user"></i>প্রশিক্ষক ম্যানেজমেন্ট<i
                            class="fa fa-angle-down rotate-icon"></i></a>
                <div class="collapsible-body">
                    <ul>
                        <li><a href="{{ url('/admin/trainer/create') }}" class="waves-effect">+ নতুন প্রশিক্ষক যোগ করুন</a>
                        </li>
                        <li><a href="{{ url('/admin/trainer') }}" class="waves-effect">সকল প্রশিক্ষকের তালিকা</a>
                        </li>
                    </ul>
                </div>
            </li>
            <li><a class="collapsible-header waves-effect arrow-r"><i class="fa fa-book"></i>কোর্স ম্যানেজমেন্ট<i
                            class="fa fa-angle-down rotate-icon"></i></a>
                <div class="collapsible-body">
                    <ul>
                        <li><a href="{{ url('/admin/course/create') }}" class="waves-effect">+ নতুন কোর্স যোগ করুন</a>
                        </li>
                        <li><a href="{{ url('/admin/course') }}" class="waves-effect">সকল কোর্সের তালিকা</a>
                        </li>
                    </ul>
                </div>
            </li>

            <li><a class="collapsible-header waves-effect arrow-r"><i class="fa fa-users"></i>শিক্ষার্থী ম্যানেজমেন্ট<i
                            class="fa fa-angle-down rotate-icon"></i></a>
                <div class="collapsible-body">
                    <ul>
                        <li><a href="modals.html" class="waves-effect"> শিক্ষার্থী ফলাফল যোগ করুন</a>
                        </li>
                        <li><a href="page-create.html" class="waves-effect">সকল ফলাফলের তালিকা</a>
                        </li>
                    </ul>
                </div>
            </li>
        </ul>
    </li>
    <!--/. Side navigation links -->
    <div class="sidenav-bg mask-strong"></div>
</ul>