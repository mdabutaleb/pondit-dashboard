<header>
    <!-- Sidebar navigation -->
@include('admin.layouts.partials.sidebar_menu')
<!--/. Sidebar navigation -->

    <!-- Navbar -->
    <nav class="navbar fixed-top navbar-toggleable-md navbar-dark scrolling-navbar double-nav">

        <!-- SideNav slide-out button -->
        <div class="float-left">
            <a href="#" data-activates="slide-out" class="button-collapse"><i class="fa fa-bars"></i></a>

        </div>

        <!-- Breadcrumb-->
        <ol class="breadcrumb hidden-lg-down">
            <li class="breadcrumb-item active"><a href="{{ url('/dashboard') }}">ড্যাশবোর্ড</a></li>
            @yield('bread_crumb')

        </ol>

        <!--Navbar links-->
        <ul class="nav navbar-nav nav-flex-icons ml-auto">

            {{--<li class="nav-item">--}}
            {{--<a class="nav-link">--}}
            {{--<span class="badge red">9 </span> <i class="fa fa-upload"></i>--}}
            {{--<span class="hidden-sm-down">Publish changes</span>--}}
            {{--</a>--}}
            {{--</li>--}}
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown"
                   aria-haspopup="true" aria-expanded="false">
                    <span class="badge red">99</span> <i class="fa fa-bell"></i>
                    <span class="hidden-sm-down">নটিফিকেশন</span>
                </a>
                <div class="dropdown-menu header-notifications animated dropdown-menu-right"
                     aria-labelledby="navbarDropdownMenuLink">
                    <ul>
                        <li class="active-notification"><i class="fa fa-user" aria-hidden="true"></i> New order received
                            <span class="float-right grey-text"><i class="fa fa-clock-o" aria-hidden="true"></i> 13 min</span>
                        </li>
                        <li class="active-notification"><i class="fa fa-user" aria-hidden="true"></i> New order received
                            <span class="float-right grey-text"><i class="fa fa-clock-o" aria-hidden="true"></i> 51 min</span>
                        </li>
                        <li class="active-notification"><i class="fa fa-bullhorn" aria-hidden="true"></i> Your campaign
                            is about to end <span class="float-right grey-text"><i class="fa fa-clock-o"
                                                                                   aria-hidden="true"></i> 3 hours</span>
                        </li>
                        <li class="active-notification"><i class="fa fa-line-chart" aria-hidden="true"></i> Raport is
                            ready to be downloaded <span class="float-right grey-text"><i class="fa fa-clock-o"
                                                                                          aria-hidden="true"></i> 1 day</span>
                        </li>
                        <li class="active-notification"><i class="fa fa-life-saver" aria-hidden="true"></i> Something
                            else <span class="float-right grey-text"><i class="fa fa-clock-o" aria-hidden="true"></i> 1 day</span>
                        </li>
                    </ul>
                </div>
            </li>
            <li class="nav-item">
                <a class="nav-link"><i class="fa fa-envelope"></i> <span class="hidden-sm-down">যোগাযোগ</span></a>
            </li>
            <li class="nav-item">
                <a class="nav-link"><i class="fa fa-comments-o"></i> <span class="hidden-sm-down">সাপোর্ট</span></a>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="userDropdown" data-toggle="dropdown"
                   aria-haspopup="true" aria-expanded="false">
                    <i class="fa fa-user"></i> <span
                            class="hidden-sm-down">{{ isset(Auth::user()->name) ? Auth::user()->name : 'User Not Found'  }}</span>
                </a>
                <div class="dropdown-menu dropdown-ins dropdown-menu-right" aria-labelledby="userDropdown">
                    <a href="{{ route('logout') }}" class="dropdown-item"
                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                        Logout
                    </a>

                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        {{ csrf_field() }}
                    </form>
                    {{--<a class="dropdown-item" href="#">Log Out</a>--}}
                    <a class="dropdown-item" href="#">My account</a>
                </div>
            </li>
        </ul>
        <!--/Navbar links-->

    </nav>
    <!-- /.Navbar -->

</header>