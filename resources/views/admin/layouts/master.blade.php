@include('admin.layouts.partials.head')

<body class="fixed-sn white-skin">

<!--Double navigation-->
@include('admin.layouts.partials.header')
<!--/.Double navigation-->


<!--Main layout-->
<main class="">
    <div class="container-fluid">
        @yield('content')

        <!--Section heading-->
    {{--<h4 class="text-left">Your business is growing</h4>--}}
    {{--<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. </p>--}}
    {{--<hr>--}}
    {{--<br>--}}

    <!--Section: Intro-->
    @yield('statistics')
    <!--/Section: Intro-->

        <!--Section: Main Chart-->

        <!--/Section: Main chart-->

        <!--Section: Charts-->

        <!--/Section: Charts-->

        <!--Section: Notifications-->
    @yield('notification')
    <!--/Section: Notifications-->

        <!--Classic student cards-->
        <!--/Classic student cards-->


    </div>
</main>
<!--/Main layout-->


<!-- SCRIPTS -->

<!-- JQuery -->
<script type="text/javascript" src="{{ asset('js/jquery-3.1.1.min.js') }}"></script>
<!-- Bootstrap tooltips -->
<script type="text/javascript" src="{{ asset('js/tether.min.js') }}"></script>

<!-- Bootstrap core JavaScript -->
<script type="text/javascript" src="{{ asset('js/bootstrap.min.js') }}"></script>

<!-- MDB core JavaScript -->
<script type="text/javascript" src="{{ asset('js/mdb.min.js') }}"></script>

<script>
    $(function () {
        var data = {
            labels: ["January", "February", "March", "April", "May", "June", "July"],
            datasets: [{
                label: "My First dataset",
                fillColor: "rgba(220,220,220,0.2)",
                strokeColor: "rgba(220,220,220,1)",
                pointColor: "rgba(220,220,220,1)",
                pointStrokeColor: "#fff",
                pointHighlightFill: "#fff",
                pointHighlightStroke: "rgba(0,0,0,.15)",
                data: [65, 59, 80, 81, 56, 55, 40],
                backgroundColor: "#4CAF50"
            }, {
                label: "My Second dataset",
                fillColor: "rgba(255,255,255,.25)",
                strokeColor: "rgba(255,255,255,.75)",
                pointColor: "#fff",
                pointStrokeColor: "#fff",
                pointHighlightFill: "#fff",
                pointHighlightStroke: "rgba(0,0,0,.15)",
                data: [28, 48, 40, 19, 86, 27, 90]
            }]
        };


        var dataPie = [{
            value: 300,
            color: "#F7464A",
            highlight: "#FF5A5E",
            label: "Red"
        }, {
            value: 50,
            color: "#46BFBD",
            highlight: "#5AD3D1",
            label: "Green"
        }, {
            value: 100,
            color: "#FDB45C",
            highlight: "#FFC870",
            label: "Yellow"
        }]

        var option = {
            responsive: true,
            // set font color
            scaleFontColor: "#fff",
            // font family
            defaultFontFamily: "'Roboto', sans-serif",
            // background grid lines color
            scaleGridLineColor: "rgba(255,255,255,.1)",
            // hide vertical lines
            scaleShowVerticalLines: false,
        };

        //Get the context of the canvas element we want to select
        // var ctx = document.getElementById("sales").getContext('2d');
        // var myLineChart = new Chart(ctx).Line(data, option); //'Line' defines type of the chart.

        // Get the context of the canvas element we want to select
        var ctx = document.getElementById("conversion").getContext('2d');
        var myRadarChart = new Chart(ctx).Radar(data, option);

        // Get the context of the canvas element we want to select
        var ctx = document.getElementById("traffic").getContext('2d');
        var myBarChart = new Chart(ctx).Bar(data, option);

        // Get the context of the canvas element we want to select
        var ctx = document.getElementById("seo").getContext('2d');
        var myPieChart = new Chart(ctx).Pie(dataPie, option);

    });
</script>

<script>
    $(function () {
        $('.min-chart#chart-sales').easyPieChart({
            barColor: "#4caf50",
            onStep: function (from, to, percent) {
                $(this.el).find('.percent').text(Math.round(percent));
            }
        });
    });
</script>

<script>
    // Data Picker Initialization
    $('.datepicker').pickadate();


    // Material Select Initialization
    $(document).ready(function () {
        $('.mdb-select').material_select();
    });

    // Sidenav Initialization
    $(".button-collapse").sideNav();

    // Tooltips Initialization
    $(function () {
        $('[data-toggle="tooltip"]').tooltip()
    })
</script>


<!-- JVectorMap -->

<link rel="stylesheet" href="{{ asset('js/vendor/jvectormap/jquery-jvectormap-2.0.3.css')}}" type="text/css" media="screen"/>
<script src="{{ asset('js/vendor/jvectormap/jquery-jvectormap-2.0.3.min.js')}}"></script>
<script src="{{ asset('js/vendor/jvectormap/jquery-jvectormap-world-mill.js')}}"></script>


<script>
    $(function () {
        $('#world-map').vectorMap({map: 'world_mill_en', zoomOnScroll: false});
    });
</script>
<!-- /.JVectorMap -->

</body>

</html>