<?php
return [
    'company_path' => 'img/company/',
    'trainer_path' => 'img/trainer/',
    'partner_path' => 'img/partner/',
    'institute_path' => 'img/institute/',
    'course_path' => 'upload/course/',
];